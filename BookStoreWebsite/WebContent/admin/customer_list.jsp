<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Manage Customers</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/style.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>

</head>
<body>
	<jsp:directive.include file="header.jsp" />
	<div align="center">
		<h2 class="pageheading">Customers Manegement</h2>
		<h3>
			<a href="new_customer">Create new Customer</a>
		</h3>
	</div>
	
	<c:if test="${message!=null}">
	
	<div align="center">

		<h4 class="message" > ${message} </h4>
	</div>
	</c:if>

	<div align="center">
		<table border="1" cellpadding="5">
			<tr>
				<th>Index</th>
				<th>ID</th>
				<th>E-mail</th>
				<th>Frist Name</th>
				<th>Last Name</th>
				<th>City</th>
				<th>Country</th>
				<th>registered Date</th>
				<th>Actions</th>


			</tr>

			<c:forEach var="customer" items="${listCustomer}" varStatus="status">
				<tr>
					<td>${status.index+1}</td>
					<td>${customer.customerId}</td>
					<td>${customer.email}</td>
					<td>${customer.firstname}</td>
					<td>${customer.lastname}</td>
					<td>${customer.city}</td>
					<td>${customer.countryName}</td>
					<td><fmt:formatDate pattern="MM/dd/yyyy" value="${customer.registerDate}"/></td>
					
					<td>
					<a href="edit_customer?id=${customer.customerId}">Edit</a>&nbsp
					<a href="javascript:void(0);" class="deleteLink" id="${customer.customerId}">Detele</a>
					</td>


				</tr>

			</c:forEach>


		</table>
	</div>



	<jsp:directive.include file="footer.jsp" />
	<script type="text/javascript">
	$(document).ready(function(){
		$(".deleteLink").each(function(){
			$(this).on("click",function(){
				customerId=$(this).attr("id");
				if(confirm('Are you sure to wanna delete the customer with ID '+customerId+ ' ?')){
					window.location='delete_customer?id='+customerId;
				}
			});
		});
	});
	
	
	</script>
</body>
</html>
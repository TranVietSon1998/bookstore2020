<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>
	<div align="center">
		<div>
		<a href="${pageContext.request.contextPath }/admin/">
			<img alt=""
				src="<%=request.getContextPath()%>/images/logo1.png">
				</a>
		</div>
		<div>
			Welcome,<c:out value="${sessionScope.useremail}" /> | <a href="logout">Logout</a>
			<br/> <br/>
		</div>
		<div id="headermenu">

			<div class="menu_item" >
				<a href="list_users"> <img alt=""
					src="<%=request.getContextPath()%>/images/user.png"><br>Users
				</a>
			</div>
			<div class="menu_item" >
				<a href="list_category"> <img alt=""
					src="<%=request.getContextPath()%>/images/category.png"><br>Categories
				</a>
			</div>
			<div class="menu_item" >
				<a href="list_books"> <img alt=""
					src="<%=request.getContextPath()%>/images/book.png"><br>Books
				</a>
			</div>
			<div class="menu_item" >
				<a href="list_customer"> <img alt=""
					src="<%=request.getContextPath()%>/images/customer.png"><br>Customers
				</a>
			</div >
			<div class="menu_item" >
				<a href="list_review"> <img alt=""
					src="<%=request.getContextPath()%>/images/review.png"><br>Reviews
				</a>
			</div>
			<div class="menu_item" >
				<a href="list_order"> <img alt=""
					src="<%=request.getContextPath()%>/images/order.png"><br>Orders
				</a>
			</div>







		</div>
	</div>

</body>
</html>
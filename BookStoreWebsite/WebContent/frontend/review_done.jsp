<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Review Posted</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
</head>
<body>
	<jsp:directive.include file="header.jsp" />
	<div align="center">

		<table width="60%">
			<tr>

				<td>
					<h2>Your Review</h2>
				</td>
				<td>&nbsp;</td>
				<td><h2>${loggedCustomer.fullname}</h2></td>

			</tr>
			<tr>
				<td colspan="3">
					<hr />
				</td>

			</tr>

			<tr>
				<td><span id="book-title">${book.title} </span> <br> <img
					class="book-large" src="data:image/jpg;base64,${book.base64Image }" />

				</td>

				<td colspan="2">
				<h3>Your review has been posted. Thank you!</h3>
				</td>
			</tr>





		</table>




	</div>
	<jsp:directive.include file="footer.jsp" />
</body>

</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books in <c:out value="${category.name}" /> - Online
	Books Store
</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css">
</head>
<body>
	<jsp:directive.include file="header.jsp" />

	<div class="center">
		<h2>${category.name}</h2>


	</div>
	<div class="book_group">
		<c:forEach items="${listBooks}" var="book">
			<div class="book">
				<div>
					<a href="view_book?id=${book.bookId}"> <img class="book-small"
						src="data:image/jpg;base64,${book.base64Image }" />
					</a>
				</div>
				<div>
					<a href="view_book?id=${book.bookId}"> <b>${book.title } </b>
					</a>
				</div>
				<div>
				
				<c:forTokens items="${book.ratingStars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				<c:if test="${star eq 'half'}">
				<img src="<%=request.getContextPath()%>/images/rating_half.png">
				</c:if>
				
				</c:forTokens>
				
				</div>
				<div>
					<i>by ${book.author} </i>
				</div>
				<div>
					<b>$${book.price}</b>
				</div>
			</div>


		</c:forEach>
	</div>
	<jsp:directive.include file="footer.jsp" />
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Result for ${keyword}</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css">
</head>
<body>
	<jsp:directive.include file="header.jsp" />

	<div class="center">
		<c:if test="${fn:length(result)==0 }">
			<h2>No result for the "${keyword}"</h2>
		</c:if>

		<c:if test="${fn:length(result)>0 }">



			<div class="book-search">
				<center><h2 >Search result for "${keyword}" :</h2> </center>
				<c:forEach items="${result}" var="book">
					<div>
						<div id="search-image">
							<div>
								<a href="view_book?id=${book.bookId}"> <img class="book-small"
									src="data:image/jpg;base64,${book.base64Image }"  />
								</a>
							</div>

						</div>
						<div id="search-description">
							<div>
								<h2><a href="view_book?id=${book.bookId}"> <b>${book.title }
								</b>
								</a>
								</h2>
								
							</div>
							<div>
							
				<c:forTokens items="${book.ratingStars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				<c:if test="${star eq 'half'}">
				<img src="<%=request.getContextPath()%>/images/rating_half.png">
				</c:if>
				
				</c:forTokens>
							
							</div>
							<div>
								<i>by ${book.author} </i>
							</div>
							<div>
								<p>${ fn:substring(book.description,0,100) }.... </p>
							</div>

						</div>
						<div id="search-price">
						<h3><b>$${book.price}</b></h3>
						<h3><a href="">Add To Cart</a> </h3>
						</div>
					</div>
				</c:forEach>
			</div>
		</c:if>



	</div>
	<jsp:directive.include file="footer.jsp" />
</body>
</html>
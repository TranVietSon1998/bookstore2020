<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${book.title}</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css">
	<script type="text/javascript"

	src="<%=request.getContextPath()%>/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>
</head>
<body>
	<jsp:directive.include file="header.jsp" />

	<div class="center1">
		<table class="book">
			<tr>
				<td colspan="3" align="left">

					<p id="book-title">${book.title } </p>
					 by <span id="author"> ${book.author }</span>

				</td>
			</tr>

			<tr>
				<td rowspan="2"><img class="book-large"
					src="data:image/jpg;base64,${book.base64Image }"  /></td>
				<td valign="top" align="left">
				<c:forTokens items="${book.ratingStars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				<c:if test="${star eq 'half'}">
				<img src="<%=request.getContextPath()%>/images/rating_half.png">
				</c:if>
				
				</c:forTokens>
				
				<a href="#reviews">${fn:length(book.reviews) } Reviews</a>
				</td>
				<td valign="top" rowspan="2" width="20%"><h2>$${book.price }</h2> <br />
					<button id="buttonAddToCart">Add to cart</button>
				</td>

			</tr>
			<tr>
				<td id="description">${book.description }</td>
			</tr>
			
			<tr>
			<td> &nbsp; </td>
			</tr>
			<tr>
			<td>
			<h2><a id="reviews">Customer Reviews </a></h2>
			</td>
			<td colspan="2" align="center">
			<button id="buttonWriteReview">Write a Customer Review</button>
			
			</td>
			
			</tr>
			<td colspan="3" align="left">
			<table>
			<c:forEach items="${book.reviews}" var="review">
			<tr>
			<td>
			<c:forTokens items="${review.stars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				
				</c:forTokens>
			-<b>${review.headline }</b>
			</td>
			
			
			</tr>
			<tr>
			<td>
			by <i> ${review.customer.fullname } </i> on ${review.reviewTime}
			</td>
			
			</tr>
			
			<tr> <td> <i>${review.comment } </i>    </td>   </tr>
			<tr> <td> &nbsp; </td>  </tr>
			
			
			
			</c:forEach>
			
			
			
			</table>
			
			</td>
			<tr>
			
			
			
			</tr>
		</table>



	</div>
	<jsp:directive.include file="footer.jsp" />
	<script type="text/javascript">
	$(document).ready(function() {
		
		$("#buttonWriteReview").click(function() {
			window.location='write_review?book_id=' +${book.bookId};
			
		});
		
		$("#buttonAddToCart").click(function() {
			window.location='add_to_cart?book_id=' +${book.bookId};
			
		});
	
	
	});
</script>
</body>
</html>

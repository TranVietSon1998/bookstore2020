<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome to My Website</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/style.css">
</head>
<body>
<jsp:directive.include file="header.jsp"/>

<div class="center">
<div >
<h4>New Books:</h4>
		<c:forEach items="${listNewBooks}" var="book">
			<div class="book">
				<div>
					<a href="view_book?id=${book.bookId}"> <img class="book-small"
						src="data:image/jpg;base64,${book.base64Image }"  />
					</a>
				</div>
				<div>
					<a href="view_book?id=${book.bookId}"> <b>${book.title } </b>
					</a>
				</div>
				<div>
				<c:forTokens items="${book.ratingStars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				<c:if test="${star eq 'half'}">
				<img src="<%=request.getContextPath()%>/images/rating_half.png">
				</c:if>
				
				</c:forTokens>
				
				
				</div>
				<div>
					<i>by ${book.author} </i>
				</div>
				<div>
					<b>$${book.price}</b>
				</div>
			</div>


		</c:forEach>
	</div>
	<div class="next-row">
<h4>Best-selling books:</h4>

<c:forEach items="${listBestSellingBooks}" var="book">
			<div class="book">
				<div>
					<a href="view_book?id=${book.bookId}"> <img class="book-small"
						src="data:image/jpg;base64,${book.base64Image }"  />
					</a>
				</div>
				<div>
					<a href="view_book?id=${book.bookId}"> <b>${book.title } </b>
					</a>
				</div>
				<div>
				<c:forTokens items="${book.ratingStars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				<c:if test="${star eq 'half'}">
				<img src="<%=request.getContextPath()%>/images/rating_half.png">
				</c:if>
				
				</c:forTokens>
				
				
				</div>
				<div>
					<i>by ${book.author} </i>
				</div>
				<div>
					<b>$${book.price}</b>
				</div>
			</div>


		</c:forEach>

</div>
<div class="next-row">
<h4>Most-favor books:</h4>
<c:forEach items="${listFavoredBooks}" var="book">
			<div class="book">
				<div>
					<a href="view_book?id=${book.bookId}"> <img class="book-small"
						src="data:image/jpg;base64,${book.base64Image }"  />
					</a>
				</div>
				<div>
					<a href="view_book?id=${book.bookId}"> <b>${book.title } </b>
					</a>
				</div>
				<div>
				<c:forTokens items="${book.ratingStars}" delims="," var="star">
				<c:if test="${star eq 'on'}">
				<img src="<%=request.getContextPath()%>/images/rating_on.png">
				</c:if>
				<c:if test="${star eq 'off'}">
				<img src="<%=request.getContextPath()%>/images/rating_off.png">
				</c:if>
				<c:if test="${star eq 'half'}">
				<img src="<%=request.getContextPath()%>/images/rating_half.png">
				</c:if>
				
				</c:forTokens>
				
				
				</div>
				<div>
					<i>by ${book.author} </i>
				</div>
				<div>
					<b>$${book.price}</b>
				</div>
			</div>


		</c:forEach>
</div>
<br><br>


</div>
<jsp:directive.include file="footer.jsp"/>
</body>
</html>
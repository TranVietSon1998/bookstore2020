package com.bookstore.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.bookstore.entity.Users;

public class UserDAOTest {

	private static UserDAO userDAO;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		userDAO = new UserDAO();
	}

	@Test
	public void testCreateUsers() {
		Users user = new Users();
		user.setEmail("nguyenngocquy1111111.com");
		user.setFullName("nguyenngoctquy11111");
		user.setPassword("quy111");

		user = userDAO.create(user);

		assertTrue(user.getUserId() > 0);

	}

	@Test(expected = PersistenceException.class)
	public void testCreateUsersFieldsNotSet() {
		Users user = new Users();

		user = userDAO.create(user);

		assertTrue(user.getUserId() > 0);

	}

	@Test
	public void testUpdateUser() {
		Users user = new Users();
		user.setUserId(1);
		user.setEmail("test");
		user.setFullName("son");
		user.setPassword("11234");
		userDAO.update(user);
		String expected = "test";
		String actual = user.getEmail();
		assertEquals(expected, actual);

	}

	@Test
	public void testGetUserFound() {
		Integer userId = 20;
		Users user = userDAO.get(userId);
		if (user != null) {
			System.out.println(user.getEmail());
		}
		assertNotNull(user);
	}

	@Test
	public void testDeleteUser() {
		Integer userId = 1;
		userDAO.delete(userId);
		Users user = userDAO.get(userId);
		assertNull(user);
	}

	@Test(expected = Exception.class)
	public void testDeleteNonExistsUser() {
		Integer userId = 100;
		userDAO.delete(userId);

	}

	@Test
	public void testListAll() {
		List<Users> listUser = userDAO.listAll();
		for (Users user : listUser) {
			System.out.println(user.getEmail());
		}
		System.out.println(listUser.size());
		assertTrue(listUser.size() > 0);
	}

	@Test
	public void testCount() {
		long totalUser = userDAO.count();
		assertTrue(totalUser == 16);
	}

	@Test
	public void testFindByEmail() {
		String email = "1123111@gmail.com";
		Users users = userDAO.findByEmail(email);
		assertTrue(users.getEmail().equalsIgnoreCase(email));
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		userDAO.close();
	}

	@Test
	public void testCheckLoginSuccess() {
		String email = "same@gmail.com";
		String password = "123";
		boolean loginResult = userDAO.checkLogin(email, password);
		assertTrue(loginResult);
	}
}

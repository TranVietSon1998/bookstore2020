package com.bookstore.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.bookstore.entity.Book;
import com.bookstore.entity.Customer;
import com.bookstore.entity.Review;

class ReviewDAOTest {
	private static ReviewDAO reviewDAO;
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		reviewDAO= new ReviewDAO();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		reviewDAO.close();
	}

	@Test
	void testCreateReview() {
		Review review= new Review();
		Book book= new Book();
		book.setBookId(40);
		Customer customer= new Customer();
		customer.setCustomerId(2);
		review.setBook(book);
		review.setCustomer(customer);
		review.setHeadline("this is very book");
		review.setRating(2);
		review.setComment("very nice book");
		Review savedReview=reviewDAO.create(review);
		assertTrue(savedReview.getReviewId()>0);
		
	
	}
	@Test
	void testGet() {
		Integer reviewId=1;
		Review review=reviewDAO.get(reviewId);
		assertTrue(review.getReviewId()>0);
		
		
		
	}
	@Test
	void testUpdate() {
		Integer reviewId=1;
		Review review=reviewDAO.get(reviewId);
		review.setHeadline("ahuuuuuuuu");
		reviewDAO.update(review);
		
	}
	@Test
	void testListAll() {
		List<Review> listReview=reviewDAO.listAll();
		for(Review abc:listReview) {
			System.out.println(abc.getHeadline());
		}
		assertTrue(listReview.size()>0);
	}
	@Test
	void testCountAll() {
		long total=reviewDAO.count();
		assertTrue(total==11);
	}
	@Test
	void testDelete() {
		reviewDAO.delete(1);
		long total=reviewDAO.count();
		
		
	}
	@Test
	public void testFindByCustomerAndBookNotFound() {
		Integer customerId=100;
		Integer bookId=100;
		Review review=reviewDAO.findByCustomerAndBook(customerId, bookId);
		assertNull(review);
		
		
	}
	@Test
	public void testFindByCustomerAndBookFound() {
		Integer customerId=2;
		Integer bookId=37;
		Review review=reviewDAO.findByCustomerAndBook(customerId, bookId);
		assertNotNull(review);
		
		
	}
	

}

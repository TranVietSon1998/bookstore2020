package com.bookstore.dao;

import com.bookstore.entity.Category;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.bookstore.entity.Category;

class CategoryDAOTest {
	private static CategoryDAO categoryDAO;

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		categoryDAO = new CategoryDAO();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		categoryDAO.close();
	}

	@Test
	void testCreateCategory() {
		Category newCat = new Category("C#");
		Category category = categoryDAO.create(newCat);
		assertTrue(category != null && category.getCategoryId() > 0);
	}
	@Test
	void testUpdateCategory() {
		Category cat=new Category("C##");
		cat.setCategoryId(1);
		 Category category =categoryDAO.update(cat);
//		 assertEquals(cat.getName(),category.getName());
assertTrue(category.getName().equalsIgnoreCase("C##"));


	}

	@Test
	public void testGet() {
		Integer catId=1;
		Category category=categoryDAO.get(catId);
		assertTrue(category.getName().equalsIgnoreCase("C##"));

	}

	@Test
	void testDeleteCategory() {
		Integer catId=1;
		categoryDAO.delete(catId);
		Category category=categoryDAO.get(catId);
		assertNull(category);

	}

	@Test
	void testListAll() {
		List<Category> listCategory=categoryDAO.listAll();
		listCategory.forEach(c->System.out.println(c.getName()+" , "));
		assertTrue(listCategory.size()>0);
	}

	@Test
	void testCount() {
		long totalCategories=categoryDAO.count();
		assertTrue(totalCategories==5);
	}
	@Test
	public void testFindByName() {
		String name="Java";
		Category category=categoryDAO.findByName(name);
		assertNotNull(category);
	}

	@Test
	public void testFindByNameNotFound() {
		String name="Java1";
		Category category=categoryDAO.findByName(name);
		assertNull(category);
	}

}

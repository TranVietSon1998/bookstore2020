package com.bookstore.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.bookstore.entity.Customer;

class CustomerDAOTest {
	private static CustomerDAO customerDAO;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		customerDAO= new CustomerDAO();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		customerDAO.close();
	}

	@Test
	void testCreateCustomer() {
		Customer customer= new Customer();
		customer.setEmail("lamconghau@gmail.com");
		customer.setFirstname("lam cong ");
		customer.setLastname("hau");
		customer.setCity("bien hoa");
		customer.setState("dong nai");
		customer.setCountry("viet nam");
		customer.setAddressLine1("17 thuc duc");
		customer.setAddressLine2("18 thuc duc");
		customer.setPassword("12345");
		customer.setPhone("0932191045");
		customer.setZipcode("100000");
		Customer savedCustomer= customerDAO.create(customer);
		assertTrue(savedCustomer.getCustomerId()>0);
	}
	@Test
	public void testGet() {
		Integer customerId=2;
		Customer customer=customerDAO.get(customerId);
		assertTrue(customer.getCustomerId()>0);
	}
	@Test
	public void testDeleteCustomer() {
		Integer customerId=1;
		customerDAO.delete(customerId);
		
	}
	@Test
	public void testUpdate() {
		Integer customerId=5;
		Customer customer=customerDAO.get(customerId);
		customer.setAddressLine1("19 linh trung");
		customerDAO.update(customer);
		
	}
	@Test
	public void testListAll() {
	List<Customer> list=	 customerDAO.listAll();
	for(Customer cus:list) {
		System.out.println(cus.getEmail());
	}
	assertTrue(list.size()>0);
	}
	@Test
	public void testCountAll() {
		long result=customerDAO.count();
		assertTrue(result==3);
	}
	@Test
	public void testFindByEmail() {
		String email="01263519039son@gmail.com";
		Customer customer=customerDAO.findByEmail(email);
		assertNotNull(customer);
	}
	@Test
	public void testCheckLogin() {
		String email="01263519039son@gmail.com";
		String password="1";
		Customer customer=customerDAO.checkLogin(email, password);
		assertNotNull(customer);
		
	}
	

}

package com.bookstore.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.bookstore.entity.Book;
import com.bookstore.entity.Category;

class BookDAOTest {
	private static BookDAO bookDao;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		bookDao = new BookDAO();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		bookDao.close();
	}

	@Test
	void testCreateBook() throws ParseException, IOException {
		Book newBook = new Book();
		Category category = new Category("Java");
		category.setCategoryId(2);
		newBook.setCategory(category);
		newBook.setTitle("Effective Java (5nd Edition)");
		newBook.setAuthor("Joshua Bloch");
		newBook.setDescription(
				"This highly anticipated new edition of the classic, Jolt Award-winning work has been thoroughly "
						+ "updated to cover Java SE 5 and Java SE 6 features introduced since the first edition. Bloch explores new design"
						+ " patterns and language idioms, "
						+ "showing you how to make the most of features ranging from generics to enums, annotations to autoboxing.");
		newBook.setPrice(31f);
		newBook.setIsbn("0321356683");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date publishDate = dateFormat.parse("05/28/2008");
		newBook.setPublishDate(publishDate);
		String imagePath = "E:\\LapTrinhJavaWeb\\bookstore2020\\books\\Effective Java.jpg";
		byte[] imagesBytes = Files.readAllBytes(Paths.get((imagePath)));
		newBook.setImage(imagesBytes);
		Book createBook = bookDao.create(newBook);
		assertTrue(createBook.getBookId() > 0);

	}

	@Test
	void testUpdateBook() throws ParseException, IOException {
		Book existBook = new Book();
		existBook.setBookId(3);
		Category category = new Category("Java2");
		category.setCategoryId(7);
		existBook.setCategory(category);
		existBook.setTitle("Effective Java (3rd Edition)");
		existBook.setAuthor("Joshua Bloch");
		existBook.setDescription(
				"This highly anticipated new edition of the classic, Jolt Award-winning work has been thoroughly "
						+ "updated to cover Java SE 5 and Java SE 6 features introduced since the first edition. Bloch explores new design"
						+ " patterns and language idioms, "
						+ "showing you how to make the most of features ranging from generics to enums, annotations to autoboxing.");
		existBook.setPrice(41.f);
		existBook.setIsbn("0321356683");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date publishDate = dateFormat.parse("05/28/2008");
		existBook.setPublishDate(publishDate);
		String imagePath = "E:\\LapTrinhJavaWeb\\bookstore2020\\books\\Effective Java.jpg";
		byte[] imagesBytes = Files.readAllBytes(Paths.get((imagePath)));
		existBook.setImage(imagesBytes);
		Book updateBook = bookDao.update(existBook);
		assertTrue(updateBook.getBookId() > 0);

	}

	@Test
	public void testDeleteBook() {
		Integer bookId = 5;
		bookDao.delete(bookId);
		assertTrue(true);
	}

	@Test
	public void testGetBook() {
		Integer bookId = 3;
		bookDao.get(bookId);
		assertTrue(true);
	}

	@Test
	public void testListAll() {
		List<Book> listBooks = bookDao.listAll();
		for (Book aBook : listBooks) {
			System.out.println(aBook.getTitle());
		}
		assertTrue(listBooks.size() > 0);
	}

	@Test
	public void testFindByTitle() {
		String title = "Effective Java (5nd Edition)";
		Book book = bookDao.findByTitle(title);
		System.out.println(book.getAuthor() + " , " + book.getDescription());
		assertNotNull(book);
	}

	@Test
	public void testCount() {
		long totalBooks = bookDao.count();
		assertTrue(totalBooks == 3);
	}

	@Test
	public void testListByCategory() {
		int categoryId = 2;
		List<Book> lisBbooks = bookDao.listByCategory(categoryId);
		assertTrue(lisBbooks.size() > 0);
	}

	@Test
	public void testListNew() {
		List<Book> listNewBooks = bookDao.listNewBooks();
		for (Book aBook : listNewBooks) {
			System.out.println(aBook.getTitle());
		}
		assertTrue(listNewBooks.size() == 4);
	}

	@Test
	public void searchInTitle() {
		String keyword = "Java";
		List<Book> result = bookDao.search(keyword);
		for (Book aBook : result) {
			System.out.println(aBook.getTitle());
		}
//	assertTrue(result.size()>0);
		assertEquals(7, result.size());
	}
	@Test
	public void testListBestSellingBook() {
		List<Book> topBestSellingBooks=bookDao.listBestSellingBook();
		assertTrue(topBestSellingBooks.size()==4);
	}
	@Test
	public void testListMostFavoredBooks() {
		List<Book> result=bookDao.listMostFavoredBooks();
		for(Book book:result) {
			System.out.println(book.getTitle());
		}
		assertEquals(4,result.size());
	}
}

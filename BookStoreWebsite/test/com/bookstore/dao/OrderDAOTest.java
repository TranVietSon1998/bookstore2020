package com.bookstore.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.bookstore.entity.Book;
import com.bookstore.entity.BookOrder;
import com.bookstore.entity.Customer;
import com.bookstore.entity.OrderDetail;
import com.bookstore.entity.OrderDetailId;

class OrderDAOTest {
	private static OrderDAO orderDAO;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		orderDAO = new OrderDAO();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		orderDAO.close();
	}

	@Test
	void testCreateBookOrder() {
		BookOrder order = new BookOrder();
		Customer customer = new Customer();
		
		customer.setCustomerId(2);
		order.setCustomer(customer);
		order.setFirstname("tran viet ");
		order.setLastname("Thau");
		order.setPhone("25522822");
		order.setAddressLine2("nha trang2");
		order.setAddressLine1("nha trang1");
		order.setCity("khanh hoa");
		order.setState("khanh hoa");
		order.setCountry("viet nam");
		order.setPaymentMethod("payment");
		order.setZipcode("1231234");
		Set<OrderDetail> orderDetails = new HashSet<>();
		OrderDetail orderDetail = new OrderDetail();

		Book book = new Book(40);
		orderDetail.setBook(book);
		orderDetail.setQuantity(10);
		orderDetail.setSubtotal(99.9f);
		
		orderDetail.setBookOrder(order);

		orderDetails.add(orderDetail);
		order.setOrderDetails(orderDetails);
		order.setTax(6.0f);
		order.setShippingFee(1.0f);
		order.setSubtotal(99.9f);
		order.setTotal(106.9f);
		orderDAO.create(order);
		assertTrue(order.getOrderId() > 0);

	}
	@Test
	void testGet() {
		Integer orderId=29;
		BookOrder order=orderDAO.get(orderId);
		System.out.println(order.getCity());
		assertNotNull(order!=null);
	}

	@Test
	void testListAll() {
		List<BookOrder> list=orderDAO.listAll();
		for(BookOrder a:list) {
			System.out.println(a);
		}
	
		assertTrue(list.size()==2);
	}
	@Test
	void testUpdate() {
		int orderId=29;
		BookOrder order=orderDAO.get(orderId);
		order.setAddressLine2("nha trang 3");
		orderDAO.update(order);
	}
	@Test
	void testCount() {
		long total=orderDAO.count();
		
		assertTrue(total==7);
	}
	@Test
	void testDelete() {
		int orderId=21;
		orderDAO.delete(orderId);
		long total=orderDAO.count();
		
	}
	@Test
	public void testListByCustomer() {
		int customerId=2;
		List<BookOrder> list=orderDAO.listByCustomer(customerId);
		assertTrue(list.size()==3);
	}
	@Test
	public void testGetByIdAndCustomer() {
		int orderId=21;
		int customerId=2;
		BookOrder order=orderDAO.get(orderId, customerId);
		assertNotNull(order);
	}
	@Test
	public void testListMost() {
		List<BookOrder> order=orderDAO.listMostRecentSales();
		assertTrue(order.size()==4);
	}

}

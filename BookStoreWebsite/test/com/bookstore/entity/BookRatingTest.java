package com.bookstore.entity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

class BookRatingTest {

	@Test
	void testAverageRaring() {
		Book book= new Book();
		Set<Review> reviews= new HashSet<Review>();
		Review review1= new Review();
		review1.setRating(3);
		reviews.add(review1);
		
		Review review2= new Review();
		review2.setRating(4);
		reviews.add(review2);
		Review review3= new Review();
		review3.setRating(5);
		reviews.add(review3);
		Review review4= new Review();
		review4.setRating(2);
		reviews.add(review4);
		
		
		
		book.setReviews(reviews);
		 float  averageRating=book.getAverageRating();
		 assertTrue(averageRating==3.5);
	}
	@Test
	public void testRatingString1() {
		float averageRating=0.0f;
		Book book= new Book();
		String ratingString=book.getRatingString(averageRating);
		String expected="off,off,off,off,off";
		assertEquals(expected, ratingString); 
	}
	
	@Test
	public void testRatingString2() {
		float averageRating=5.0f;
		Book book= new Book();
		
		String ratingString=book.getRatingString(averageRating);
		String expected="on,on,on,on,on";
		assertEquals(expected, ratingString); 
	}
	
	@Test
	public void testRatingString3() {
		float averageRating=3.0f;
		Book book= new Book();
		
		String ratingString=book.getRatingString(averageRating);
		String expected="on,on,on,off,off";
		assertEquals(expected, ratingString); 
	}
	
	@Test
	public void testRatingString4() {
		float averageRating=3.5f;
		Book book= new Book();
		
		String ratingString=book.getRatingString(averageRating);
		String expected="on,on,on,half,off";
		assertEquals(expected, ratingString); 
	}
	
	@Test
	public void testRatingString5() {
		float averageRating=3.6f;
		Book book= new Book();
		
		String ratingString=book.getRatingString(averageRating);
		String expected="on,on,on,half,off";
		assertEquals(expected, ratingString); 
	}

}

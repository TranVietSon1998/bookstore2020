package com.bookstore.entity;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	public static SessionFactory getSessionFactory() {
		try {
			Configuration configuration=new Configuration();
			configuration.configure("hibernate.cfg.xml");
			 return configuration.buildSessionFactory();
		} catch (Exception e) {
			
		}
		return null;
		
		
		
	}
}

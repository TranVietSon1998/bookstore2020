package com.bookstore.controller.admin.category;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookstore.dao.CategoryDAO;
import com.bookstore.service.CategorySerices;


@WebServlet("/admin/list_category")
public class ListCagerogyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public ListCagerogyServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print("List category");
		CategorySerices categorySerices=new CategorySerices(request, response);
		categorySerices.listCategory();
	}

	

}

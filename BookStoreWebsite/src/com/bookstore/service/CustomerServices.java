package com.bookstore.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.w3c.dom.ls.LSInput;

import com.bookstore.dao.CustomerDAO;
import com.bookstore.entity.Customer;

public class CustomerServices {
	private CustomerDAO customerDAO;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CustomerServices(HttpServletRequest request, HttpServletResponse response) {
		super();
		this.request = request;
		this.response = response;
		this.customerDAO = new CustomerDAO();
	}

	public void listCustomers(String message) throws ServletException, IOException {
		List<Customer> listCustomer = customerDAO.listAll();
		if (message != null) {
			request.setAttribute("message", message);
		}
		request.setAttribute("listCustomer", listCustomer);
		String listPage = "customer_list.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(listPage);
		dispatcher.forward(request, response);

	}

	public void listCustomers() throws ServletException, IOException {
		listCustomers(null);
	}

	public void registerCustomer() throws ServletException, IOException {
		String email = request.getParameter("email");
		Customer existCustomer = customerDAO.findByEmail(email);
		String message = null;
		if (existCustomer != null) {
			message = "could not register. The email " + email + " is already registerd by another customer";
		} else {

			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String password = request.getParameter("password");
			String phone = request.getParameter("phone");
			String addressLine1 = request.getParameter("address1");
			String addressLine2 = request.getParameter("address2");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String zipCode = request.getParameter("zipCode");
			String country = request.getParameter("country");

			Customer newCustomer = new Customer();

			if (email != null && !email.equals("")) {
				newCustomer.setEmail(email);
			}

			newCustomer.setFirstname(firstname);
			newCustomer.setLastname(lastname);

			if (password != null && !password.equals("")) {
				newCustomer.setPassword(password);
			}

			newCustomer.setPhone(phone);
			newCustomer.setAddressLine1(addressLine1);
			newCustomer.setAddressLine2(addressLine2);
			newCustomer.setCity(city);
			newCustomer.setState(state);
			newCustomer.setZipcode(zipCode);
			newCustomer.setCountry(country);
			customerDAO.create(newCustomer);
			message = "You have registered successfully. Thank you. <br/>"
					+ "<a href ='login'>Click here </a> to login";

		}
		String messagePage = "frontend/message.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(messagePage);
		request.setAttribute("message", message);
		dispatcher.forward(request, response);

	}

	public void createCustomer() throws ServletException, IOException {
		String email = request.getParameter("email");
		Customer existCustomer = customerDAO.findByEmail(email);
		if (existCustomer != null) {
			String message = "could not create new customer : the email " + email
					+ " is already registerd by another customer";
			listCustomers(message);
		} else {

			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String password = request.getParameter("password");
			String phone = request.getParameter("phone");
			String addressLine1 = request.getParameter("address1");
			String addressLine2 = request.getParameter("address2");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String zipCode = request.getParameter("zipCode");
			String country = request.getParameter("country");

			Customer newCustomer = new Customer();

			if (email != null && !email.equals("")) {
				newCustomer.setEmail(email);
			}

			newCustomer.setFirstname(firstname);
			newCustomer.setLastname(lastname);

			if (password != null && !password.equals("")) {
				newCustomer.setPassword(password);
			}

			newCustomer.setPhone(phone);
			newCustomer.setAddressLine1(addressLine1);
			newCustomer.setAddressLine2(addressLine2);
			newCustomer.setCity(city);
			newCustomer.setState(state);
			newCustomer.setZipcode(zipCode);
			newCustomer.setCountry(country);
			customerDAO.create(newCustomer);
			String message = "New customer has been created successfully";
			listCustomers(message);

		}

	}

//	public void editCustomer() throws ServletException, IOException {
//		Integer customerId = Integer.parseInt(request.getParameter("id"));
//		Customer customer = customerDAO.get(customerId);
//		
//		request.setAttribute("customer", customer);
//		generateCountryList();
//		String editPage = "customer_form.jsp";
//		RequestDispatcher dispatcher = request.getRequestDispatcher(editPage);
//		dispatcher.forward(request, response);
//
//	}
//
//	public void updateCustomer() throws ServletException, IOException {
//		String message = null;
//		Integer customerId = Integer.parseInt(request.getParameter("customerId"));
//		String email = request.getParameter("email");
//		Customer existCustomer = customerDAO.findByEmail(email);
//		if (existCustomer != null && existCustomer.getCustomerId() != customerId) {
//			message = "could not update the customer ID " + customerId + " "
//					+ "because there's an extisting customer having the same email.";
//
//		} else {
//			String firstname = request.getParameter("firstname");
//			String lastname = request.getParameter("lastname");
//			String password = request.getParameter("password");
//			String phone = request.getParameter("phone");
//			String addressLine1 = request.getParameter("address1");
//			String addressLine2 = request.getParameter("address2");
//			String city = request.getParameter("city");
//			String state = request.getParameter("state");
//			String zipCode = request.getParameter("zipCode");
//			String country = request.getParameter("country");
//
//			Customer newCustomer = new Customer();
//
//			if (email != null && !email.equals("")) {
//				newCustomer.setEmail(email);
//			}
//
//			newCustomer.setFirstname(firstname);
//			newCustomer.setLastname(lastname);
//
//			if (password != null && !password.equals("")) {
//				newCustomer.setPassword(password);
//			}
//
//			newCustomer.setPhone(phone);
//			newCustomer.setAddressLine1(addressLine1);
//			newCustomer.setAddressLine2(addressLine2);
//			newCustomer.setCity(city);
//			newCustomer.setState(state);
//			newCustomer.setZipcode(zipCode);
//			newCustomer.setCountry(country);
//			customerDAO.update(newCustomer);
//			message = "The customer has been updated successfully";
//		}
//		listCustomers(message);
//
//	}
	
	public void editCustomer() throws ServletException, IOException {
		Integer customerId = Integer.parseInt(request.getParameter("id"));
		Customer customer = customerDAO.get(customerId);
		
		request.setAttribute("customer", customer);
		
		CommonUtility.generateCountryList(request);
		
		String editPage = "customer_form.jsp";
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(editPage);
		requestDispatcher.forward(request, response);		
	}

	public void updateCustomer() throws ServletException, IOException {
		Integer customerId = Integer.parseInt(request.getParameter("customerId"));
		String email = request.getParameter("email");
		
		Customer customerByEmail = customerDAO.findByEmail(email);
		String message = null;
		
		if (customerByEmail != null && customerByEmail.getCustomerId() != customerId) {
			message = "Could not update the customer ID " + customerId
					+ "because there's an existing customer having the same email.";
			
		} else {
			
			Customer customerById = customerDAO.get(customerId);
			updateCustomerFieldsFromForm(customerById);
			
			customerDAO.update(customerById);
			
			message = "The customer has been updated successfully.";
		}
		
		listCustomers(message);
	}


	public void deleteCustomer() throws ServletException, IOException {

		Integer customerId = Integer.parseInt(request.getParameter("id"));
		customerDAO.delete(customerId);
		String message = "The customer has been deteled successfully";
		listCustomers(message);
	}

	public void showLogin() throws ServletException, IOException {
		String loginPage = "frontend/login.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(loginPage);
		dispatcher.forward(request, response);

	}

	public void showForm() throws ServletException, IOException {
		generateCountryList();
		String resultPage = "frontend/register_form.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(resultPage);
		dispatcher.forward(request, response);

	}

	public void doLogin() throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Customer customer = customerDAO.checkLogin(email, password);
		if (customer == null) {
			String message = "login failed. Please check your email and password.";
			request.setAttribute("message", message);
			showLogin();
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("loggedCustomer", customer);

			Object objRedirectURL = session.getAttribute("redirectURL");

			if (objRedirectURL != null) {
				String redirectURL = (String) objRedirectURL;

				session.removeAttribute("redirectURL");

				response.sendRedirect(redirectURL);
			}

			else {

				String profilePage = "frontend/customer_profile.jsp";
				RequestDispatcher dispatcher = request.getRequestDispatcher(profilePage);
				dispatcher.forward(request, response);
			}

		}

	}

	public void showCustomerProfile() throws ServletException, IOException {
		String profilePage = "frontend/customer_profile.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(profilePage);
		dispatcher.forward(request, response);

	}

	public void showCustomerProfileEditForm() throws ServletException, IOException {
		generateCountryList();
		String editPage = "frontend/edit_profile.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(editPage);
		dispatcher.forward(request, response);

	}

	public void updateCustomerProfile() throws ServletException, IOException {
		
		Customer newCustomer = (Customer) request.getSession().getAttribute("loggedCustomer");
		String email = request.getParameter("email");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String password = request.getParameter("password");
		String phone = request.getParameter("phone");
		String addressLine1 = request.getParameter("address1");
		String addressLine2 = request.getParameter("address2");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String zipCode = request.getParameter("zipCode");
		String country = request.getParameter("country");

		if (email != null && !email.equals("")) {
			newCustomer.setEmail(email);
		}

		newCustomer.setFirstname(firstname);
		newCustomer.setLastname(lastname);

		if (password != null && !password.equals("")) {
			newCustomer.setPassword(password);
		}

		newCustomer.setPhone(phone);
		newCustomer.setAddressLine1(addressLine1);
		newCustomer.setAddressLine2(addressLine2);
		newCustomer.setCity(city);
		newCustomer.setState(state);
		newCustomer.setZipcode(zipCode);
		newCustomer.setCountry(country);
		customerDAO.update(newCustomer);
		showCustomerProfile();

	}

	public void newCustomer() throws ServletException, IOException {
		generateCountryList();

		String customerForm = "customer_form.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(customerForm);
		dispatcher.forward(request, response);

	}

	private void generateCountryList() {
		String[] countryCodes = Locale.getISOCountries();

		Map<String, String> mapCountries = new TreeMap<>();

		for (String countryCode : countryCodes) {
			Locale locale = new Locale("", countryCode);
			String code = locale.getCountry();
			String name = locale.getDisplayCountry();
			mapCountries.put(name, code);
		}
		request.setAttribute("mapCountries", mapCountries);
	}
	private void updateCustomerFieldsFromForm(Customer customer) {
		String email = request.getParameter("email");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String password = request.getParameter("password");
		String phone = request.getParameter("phone");
		String addressLine1 = request.getParameter("address1");
		String addressLine2 = request.getParameter("address2");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String zipCode = request.getParameter("zipCode");
		String country = request.getParameter("country");
		
		if (email != null && !email.equals("")) {
			customer.setEmail(email);
		}
		
		customer.setFirstname(firstname);
		customer.setLastname(lastname);
		
		if (password != null && !password.equals("")) {
			customer.setPassword(password);
		}
		
		customer.setPhone(phone);
		customer.setAddressLine1(addressLine1);
		customer.setAddressLine2(addressLine2);
		customer.setCity(city);
		customer.setState(state);
		customer.setZipcode(zipCode);
		customer.setCountry(country);		
	}

}

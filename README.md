# BookStore2020

Yêu cầu:Sài IDE Là Eclipse,Xampp để dùng Mysql và Navicat để import my db
Thứ nhất import file db Sơn gửi để có db 
Thứ hai,import project về máy,nhớ xem file pom để pik cái version,đặc biệt là hibernate và mysql connector(để sài được hibernate tools, khác phiên bản ko sài đc
, ví dụ như chỉ sài đc hibernate version 5. , 6. đổ lên k sài được, tương tự như mysql connector )
Thứ 3 là tạo hibernate tools . Link đính kèm nè : https://www.youtube.com/watch?v=Dq74HO-oYeI
À nhớ khởi tạo dữ liệu trước trong db rồi làm nha :))) À sài tomcat 9 nha!!!. Maven 3.6.2
---------------------------------------------
Task list-Create Website'Home Page
1:Create home page(index.jsp)
2:Create home page servlet(HomeServlet.java)
3:Create header(header.jsp)
--------------------------------
Task list-Create Admin's Home Page
1:Create admin home page(index.jsp)
2:Create admin home servlet(AdminHomeServlet.java)
------------------------------------
Task list-User Management 
1:Code UserDao+Unit tests
2:Implement Create User feature
3:Implement List User feature
4:Implement List User feature
5:Implement Delete User featute
----------------------------
Task list-Category Management 
1:Code CategoryDao+Unit tests
2:Implement Create Category feature
3:Implement List Category feature
4:Implement List Category feature
5:Implement Delete Category featute
6:Update Website's Home Page
-----------------------------
Task list-Using Css,Jquery
-------------------------
Task list-Implement Authentication for Admin
1:Implement checkLogin() in UserDao+Unit tests
2:Create login.jsp and AdminLoginServlet
3:Update Userservices class
4:Update Admin Header.jsp
5:Create AdminLogoutServlet
6:Create AdminLoginFilter
----------------------------------
Task list-Book Management
1:Code BookDao+Unit tests
2:Implement Create Book feature
3:Implement List Books feature
4:Implement List Books feature
5:Implement Delete Books featute
-----------------------------
List Books feature in Front-End:
List Books by Category
List Books in Home Page
+New Books - Most  recently published Books
+Best -selling Boooks
+Most- favored Books
Task list- List books by Category
1:Code listByCategory in BookDao class
2:create ViewBooksByCategoryServlet class
3:Update BookService class
4:Code List Books by Category Page(jsp)
-------------------------
Task list:List new Book in Home Page
1:Implement get() method in BookDao +Write Unit tests
2:Create  ViewBooksByCategoryServlet
3:Update BookService
4:Create Book Details Page(jSP)
-----------------------------
Task list:Search Book feature
1:Implement search() method in bookDao+ Write Unit tests
2:Create SearchBookServlet
3:Update BookService
4:Implement Search Result Page(Jsp)
----------------
Optimize code all project
1:Update jpaDao
2:Delete BaseServlet class
3:Create a new java Filter Servlet
4:Up date Css file
---------------
Task list:Small update project
1:Add hyberlink for logoImage in home page
2:Use a Simple Rich Text Editor in Book Form
3:Check Books belong to a Category before Delete a Category

----------------------
Task list:Customer Management
1:Code CustomerDao+Unit tests
2:Implement Create Customer feature
3:Implement List Customer feature
4:Implement Edit Customer feature
5:Implement Delete Customer feature
--------------
Task list:CustomerRegister
1:Create Show CustomerRegisterFormServlet
2:Code Customer Register Form
3:Create RegisterCustomerServlet
4:Update CustomerSerivce class
5:Create Message Page(jsp)
----------------------
Task list:Authentication For Customer
1:Code Customer Login Form
2:Code CustomerLoginServlet
3:Code CustomerLogoutServlet
4:Code CustomerLoginFilter
---------------------
Task list:Customer Profile
1:Code Customer Profile Page
2:Code Edit Customer Profile Page
3:Code Update Customer Profile Page
4:Create UpdateCustomerProfileServlet
5:Update CustomerServices class
----------------------
Task list:Review Management
1:Code ReviewDAO+ Unit tests
2:Implement List Reviews feature
2.1:Code ListReviewServlet
2.2:Code ReviewServices
2.3:Code Reivew List Page(HTML+JSTL)
3:Implement Update Review feature
3.1:Create EditReviewServlet class
3.2:Update Review Form Page(jsp)
3.3:Create UpdateReviewServlet
3.4:Update ReviewServices class
4:Implement Delete Review feature
4.1:Show Delte Confirm Dialog
4.2:Create DeleteServlet
4.3:Update ReviewServices
-----------------------
Task list: Display rating stars for a book in listing page
1:Calculate Average Rating of a book
2:Define data structure to display rating stars
3:Display rating stars for book in:
Home page
List Books in category page
Book detail page
---------------------------
Task list:Customer Writing Review
1:Update ReviewDAO Class+ Unit tests
2:Create WriteReviewServlet Class
3:Update ReviewServices Class
4:Create Review New Form
5:Create Review Read-Only Form
6:Create SubmitReviewServlet Class
7:Create Review Message Page
---------------------
Task list:Shopping Cart feature
1:Code ShoppingCart Class+Unit tests
2:Implement Add Book to Cart function
3:Implement Remove Book from Cart function
4:Implement Update Book in Cart function
5:Implement Clear Cart function
----------------------
Task list:Code Order Management
1:Code OrderDAO Class +Unit tests
2:Implement List Orders feature
3:Implement View Order Details
4:Update Shipping information
5:Update Quantities of Books
6:Remove Books from Order
7:Add Book To Order
8:Delete Order
------------------------
Task list:View Order History feature
1:Update OrderDAO+ Unit tests
2:Create ViewOrderServlet
3:Update OrderServices
4:Code Order History Page
5:Create ShowOrderDetailServlet
6:Code Order Detail Page
7:Update Customer Login Filter
-------------------------
Task list:Finish Website Home Page
Required:
+Best-Selling Books: Books are sold the most-->Book have the biggest number of copies(quantity)
-Reflected in order Details
+Most-favored Books: 
-Book have Average rating >=4
-Books have the most number of reviews
1:Update OrderDAO class with UnitTests
2:Update ReviewDAO class with Unit tests
3:Update HomeServlet class
4:Update index JSP
----------------
Task list:Finish Admin Home Page
1:Update hyberlink for quick actions
2:Update Order class
3:Update ReviewDAO class
4:Update Admin HomeServlet
5:Update Admin Home Page
--------------------
Task List:Update Customer Management
1:Update table Customer in database
2:Update Customer model class
3:Update CustomerDaoTest class
4:Update modules in backend:
+Admin dashboard, List customers, Create new customer, Edit customers
5:Update module in Frontend
+Customer registration, Custome's profile page, update customer's profile.
------------------
Task list:Update Order Modules for integrating PayPal payment
1: Update table book_order in database
2:Update BookOrder model class
3:Update OrderDAOTEST
4:Update modules in backend:
+Admin dashboard, Order details, Edit order
5:Update modules in Frontend
+Order details
--------------------------
Task list:PayPal payment integration
1:Create PayPal business account
2:Create PayPal Sanbox App
3:Get PayPal Rest SDK
4:Write Java code & jsp
5:Test Payment integration in Sanbox model
6:Create PayPal Live App












